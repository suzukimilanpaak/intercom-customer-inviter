Hello,

I will first explain how to install, use and test this project.
Later, I will maybe list up points worth mentioning. Finally, I will
write about my achievements.


# Install

```shell
% gem install bundler
% bundle install
```

# Usage

path to customer data: data/customers.txt
distance for finding users in km: 100

```shell
% ./find_users_within_km.sh data/customers.txt 100
4: Ian Kehoe
5: Nora Dempsey
6: Theresa Enright
8: Eoin Ahearn
11: Richard Finnegan
12: Christina McArdle
13: Olive Ahearn
15: Michael Ahearn
17: Patricia Cahill
23: Eoin Gallagher
24: Rose Enright
26: Stephen McArdle
29: Oliver Ahearn
30: Nick Enright
31: Alan Behan
39: Lisa Ahearn
```


# Test

```shell
% bundle exec rspec
% rubocop
% open coverage/index.html
```


# Things worth mentioning

## StdoutPrintStrategy

I extracted the procedure to print the result of searching and sorting users as a strategy called StdoutPrintStrategy. Benefits of this are:

- Strategies are switchable. We can have, for instance, HtmlRenderStrategy in the future
- CustomerTable doesn't keep being changed by the changes in the strategy



https://bitbucket.org/suzukimilanpaak/intercom-customer-inviter/src/master/src/customer_table.rb

#  Proudest Achievement

## Publishment

English for problem-solving and coding (Japanese)
https://www.amazon.co.jp/dp/B07KPYRG1D

I authored a book for Japanese programmers to encourage them to utilize
sense of English when coding.


## Built up new advertising schema

When I belonged to an advertisement department in Cookpad. I suggested building up a new advertising schema and reorganising old ones to my manager. Later I played the main role in the project, communicating different titles of people. Developers, including me, extracted image-resizing server to a microservices on Lambda out from a monolith of RoR for an advertising server. Also, refactored its legacy codebase. The whole process took a half year. We saw a very positive impact on revenue.


Thank you for taking time for this.