# frozen_string_literal: true

require 'radian'

# Calcurator for great circle
class GreatCircle
  RADIUS = 6371.0088

  # Get distance between two locations
  #
  # @param lat1 [Numeric] latitude of a location1
  # @param lng1 [Numeric] longitude of a location1
  # @param lat2 [Numeric] latitude of a location2
  # @param lng2 [Numeric] longitude of a location2
  #
  # @example
  #  GreatCircle.new.distance(53.339428, -6.257664, 50.054552, 19.957895)
  #  # => 1832.3691057524893
  #
  # @return [Float] A great-circle distance between given locations
  def distance(lat1, lng1, lat2, lng2)
    rad_x1 = Radian.new(lat1).value
    rad_x2 = Radian.new(lat2).value
    rad_ydistance = Radian.new(lng2 - lng1).value
    calc_distance(rad_x1, rad_x2, rad_ydistance)
  end

  private

  def calc_distance(rad_x1, rad_x2, rad_ydistance)
    Math.acos(
      Math.sin(rad_x1) * Math.sin(rad_x2) +
      Math.cos(rad_x1) * Math.cos(rad_x2) * Math.cos(rad_ydistance)
    ) * RADIUS
  end
end
