# frozen_string_literal: true

# Printing strategy for STDOUT
class StdoutPrintStrategy
  # Print given table
  #
  # @param table [Enumerable] table to be sorted
  #
  # @example
  #  users = [{ 'user_id' => 1, 'name' => 'john doe' }]
  #  StdoutPrintStrategy.print(users)
  #  # => 1: john doe
  def print(table)
    table.each do |row|
      puts "#{row['user_id']}: #{row['name']}"
    end
  end
end
