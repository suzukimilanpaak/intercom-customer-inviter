# frozen_string_literal: true

require 'csv'
require 'json'
require 'forwardable'
require 'logger'
require 'stdout_print_strategy'

# Represents CSV of customers
class CustomerTable
  HEADERS = %w[latitude user_id name longitude].freeze

  extend Forwardable

  attr_reader :file, :log

  delegate %i[to_a each map] => :table

  def initialize(file, print_strategy = StdoutPrintStrategy.new)
    @file = file
    @print_strategy = print_strategy
    @log = Logger.new($stdout)
  end

  # Sort table by specified column
  #
  # @param col [String] name of column to sort
  #
  # @example
  #  CustomerTable.new('data/test_customer.txt').sort_by(:user_id).map { |row| row['user_id'] }
  #  # => [1, 2, 12, 25]
  #
  # @return [CSV::Table] CSV::Table after sorting
  def sort_by!(col)
    col = validate_and_get_column(col)
    rows = table.sort { |a, b| a[col] <=> b[col] }
    @table = CSV::Table.new(rows)
    self
  end

  # Select rows with given strategy
  #
  # @param &block [Proc] Strategy to select rows
  #
  # @example
  #   customer_table.select do |row|
  #     lat1 = 53.339428
  #     lng1 = -6.257664
  #     lat2 = row['latitude'].to_f
  #     lng2 = row['longitude'].to_f
  #     GreatCircle.new.distance(lat1, lng1, lat2, lng2) <= 100
  #   end
  #   # => [12] # user_ids of selected customer
  #
  # @return [CSV::Table] CSV::Table after sorting
  def select!(&block)
    block = proc { |_row| true } unless block_given?
    rows = table.select(&block)
    @table = CSV::Table.new(rows)
    self
  end

  def print
    @print_strategy.print(self)
  end

  private

  def table
    return @table if defined?(@table)

    rows = []
    File.foreach(file) do |line|
      row = CSV::Row.new([], [], true)
      row << JSON.parse(line)
      rows << row
    rescue StandardError
      log.warn "Invalid Json: #{line}"
    end

    @table = CSV::Table.new(rows)
  end

  def validate_and_get_column(col)
    col = col.to_s
    raise ArgumentError, "Undefined Column: #{col}" unless table.headers.include?(col)

    col
  end
end
