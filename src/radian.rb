# frozen_string_literal: true

# Calcuurator for radian
class Radian
  attr_reader :degree

  # @param degree [Numeric] degree to be converted to radian
  #
  # @example
  #  Radian.new(1).value
  #  # => 0.017453292519943295
  #
  # @return [Float] a radian value
  def initialize(degree)
    @degree = degree
  end

  def value
    degree.to_f * Math::PI / 180.0
  end
end
