# frozen_string_literal: true

require 'spec_helper'
require 'great_circle'

describe GreatCircle do
  subject(:great_circle) { described_class.new }

  describe '#distance' do
    context 'when a point specified' do
      it { expect(great_circle.distance(0, 0, 0, 0)).to eq(0) }
    end

    context 'when all negative' do
      let(:actual) { great_circle.distance(-1, -1, -2, -2) }
      let(:expected) { great_circle.distance(1, 1, 2, 2) }

      it 'is same as all positive' do
        expect(actual).to eq(expected)
      end
    end

    context 'when two actual locations given' do
      let(:location1) { [53.339428, -6.257664] }
      let(:location2) { [50.054552, 19.957895] }
      let(:expected) { 1832.3691057524893 }

      it { expect(great_circle.distance(*location1, *location2)).to eq(expected) }
    end
  end
end
