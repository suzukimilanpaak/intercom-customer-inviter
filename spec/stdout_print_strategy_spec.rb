# frozen_string_literal: true

require 'spec_helper'
require 'stdout_print_strategy'

describe StdoutPrintStrategy do
  subject(:strategy) do
    described_class.new
  end

  describe '#print' do
    context 'when talbe with users given' do
      let(:table) do
        [{ 'user_id' => 1, 'name' => 'john doe' }]
      end

      let(:actual) do
        strategy.print(table)
      end

      it { expect { actual }.to output(/1: john doe/).to_stdout }
    end

    context 'when empty talbe given' do
      let(:table) do
        []
      end

      let(:actual) do
        strategy.print(table)
      end

      it { expect { actual }.to output('').to_stdout }
    end
  end
end
