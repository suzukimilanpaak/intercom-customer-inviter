# frozen_string_literal: true

require 'spec_helper'

describe Radian do
  describe '#value' do
    context 'when degree is 0' do
      subject(:radian) { described_class.new(0) }

      it { expect(radian.value).to eq(0) }
    end

    context 'when degree is 1' do
      subject(:radian) { described_class.new(1) }

      it { expect(radian.value).to eq(0.017453292519943295) }
    end
  end
end
