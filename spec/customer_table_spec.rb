# frozen_string_literal: true

require 'spec_helper'
require 'customer_table'
require 'great_circle'

describe CustomerTable do
  subject(:customer_table) do
    described_class.new('data/test_customer.txt')
  end

  context 'when file not exist' do
    it { expect { described_class.new('nonexistent').select! }.to raise_error(SystemCallError) }
  end

  describe '#sort_by' do
    context 'with valid column' do
      let(:actual) do
        customer_table.sort_by!(:user_id).map { |row| row['user_id'] }
      end

      it { expect(actual).to eq([1, 2, 12, 25]) }
    end

    context 'with invalid column' do
      let(:actual) do
        customer_table.sort_by!(:invalid)
      end

      it { expect { actual }.to raise_error(ArgumentError) }
    end
  end

  describe '#select_by' do
    context 'when valid selector logic provided' do
      let(:actual) do
        customer_table.select! do |row|
          lat1 = 53.339428
          lng1 = -6.257664
          lat2 = row['latitude'].to_f
          lng2 = row['longitude'].to_f
          GreatCircle.new.distance(lat1, lng1, lat2, lng2) <= 100
        end
      end

      it 'returns user within 100km distance' do
        expect(actual.map { |row| row['user_id'] }).to eq([12])
      end
    end

    context 'when valid selector logic not provided' do
      let(:actual) do
        customer_table.select!
      end

      it 'returns all users' do
        expect(actual.map { |row| row['user_id'] }).to eq([12, 1, 2, 25])
      end
    end

    context 'when selector logic returns false' do
      let(:actual) do
        customer_table.select! { |_row| false }
      end

      it 'returns empty array' do
        expect(actual.map { |row| row['user_id'] }).to eq([])
      end
    end
  end

  describe '#print' do
    context 'when printing all customers' do
      let(:expected) do
        <<~EXPECTED
          12: Christina McArdle
          1: Alice Cahill
          2: Ian McArdle
          25: David Behan
        EXPECTED
      end

      it do
        expect { customer_table.print }.to output(Regexp.new(expected)).to_stdout
      end
    end

    context 'when printing customers within 100km' do
      let(:expected) do
        '12: Christina McArdle'
      end

      let(:actual) do
        customer_table.sort_by!(:user_id).select! do |row|
          lat1 = 53.339428
          lng1 = -6.257664
          lat2 = row['latitude'].to_f
          lng2 = row['longitude'].to_f
          GreatCircle.new.distance(lat1, lng1, lat2, lng2) <= 100
        end
      end

      it do
        expect { actual.print }.to output(Regexp.new(expected)).to_stdout
      end
    end
  end
end
