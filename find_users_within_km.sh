#!/usr/bin/env ruby

# Usage:
#   path to customer data: data/customers.txt
#   distance for finding userrs in km: 100
#
#   ```shell
#   % ./find_users_within_km.sh data/customers.txt 100
#   ```

$LOAD_PATH << File.join(File.dirname(__FILE__), 'src')

require 'customer_table'
require 'great_circle'

table = CustomerTable.new(ARGV[0])

table.sort_by!(:user_id).select! do |row|
  lat1 = 53.339428
  lng1 = -6.257664
  lat2 = row['latitude'].to_f
  lng2 = row['longitude'].to_f
  GreatCircle.new.distance(lat1, lng1, lat2, lng2) <= 100
end.print
